package com.automationanywhere.professional_services.logging;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class TestLoggingSession
{

    public static final String SQL_INSERT_LOG = "CALL insert_log(?,?,?,?,?,?,?)";

    public static void main(String[] args)
            throws ClassNotFoundException
    {

        Class.forName("org.postgresql.Driver");

        try (Connection conn = DriverManager.getConnection(args[0], args[1], args[2]);)
        {

            // create

            try (PreparedStatement cstmt = conn.prepareCall(SQL_INSERT_LOG))
            {

                // set parameters
                cstmt.setString(1, "test computer name");
                cstmt.setString(2, "test username");
                cstmt.setString(3, "456");
                cstmt.setString(4, "test taskbot name");
                cstmt.setString(5, "DEBUG");
                cstmt.setString(6, "test log message");
                cstmt.setString(7, "reference");

                // execute statement
                cstmt.execute();
            }

        } catch (Exception e)
        {
            e.printStackTrace();
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Opened database successfully");

        /*
        Double processId = 123D;

        String loggingConfig = "{\"isDebugEnabled\":true,\"isLocalLoggingEnabled\":true,\"isLocalLogPruningEnabled\":true,\"isLoggingCentralisationEnabled\":true," +
                                       "\"isLoggingDatabaseEnabled\":true,\"localRpaRootFolder\":\"[REGISTRY_SHELL_FOLDERS_PERSONAL]\\\\testing\\\\Logging\\\\Local subfolder\",\"fileshareRpaRootFolder\":\"C:\\\\testing\\\\Logging\\\\Fileshare subfolder\",\"loggingDatabaseConnectionString\":\"jdbc\",\"loggingDatabaseTable\":\"Logs\",\"logFileDateFormat\":\"yyyy-MM-dd\",\"logFileDateFormatRegex\":\"\\\\d{4}-\\\\d{2}-\\\\d{2}\",\"logFileTimestampFormat\":\"yyyy-MM-dd HH-mm-ss\",\"logFileSeparator\":\"|\",\"logFilePruningDaysToRetain\":7}";

        try
        {

            // create logging session object
            LoggingSession logSession = new LoggingSession("000", loggingConfig);

            logSession.endSession();
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
         */
    }
}
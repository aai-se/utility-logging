package com.automationanywhere.professional_services.logging;

/**
 Enum representing different log levels.
 */
public enum LogLevel
{
    DEBUG, INFO, ERROR, FATAL
}
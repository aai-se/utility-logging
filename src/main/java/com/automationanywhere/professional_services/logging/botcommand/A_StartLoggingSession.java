package com.automationanywhere.professional_services.logging.botcommand;

import com.automationanywhere.bot.service.BotException;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.professional_services.logging.LoggingSession;
import com.automationanywhere.professional_services.utilities.ControlRoomApiUtility;
import com.automationanywhere.professional_services.utilities.SessionProvider;

@BotCommand
@CommandPkg
        (
                name = "StartLoggingSession",
                label = "[[StartLoggingSession.label]]",
                node_label = "[[StartLoggingSession.node_label]]",
                description = "[[StartLoggingSession.description]]",
                icon = "pkg.svg"
        )
public class A_StartLoggingSession
{
    public static final String DEFAULT_SEPARATOR = "|";
    public static final String DEFAULT_TIMESTAMP_FORMAT = "yyyy-MM-dd HH-mm-ss";

    // global session context
    @com.automationanywhere.commandsdk.annotations.GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        this.globalSessionContext = globalSessionContext;
    }


    @Execute
    public void startLoggingSession(

            @Idx(index = "1", type = AttributeType.NUMBER)
            @Pkg(label = "[[StartLoggingSession.processId.label]]", default_value_type = DataType.NUMBER)
            @NotEmpty
            Double processIdInput

    )
    {
        try
        {
            // convert process ID from number to string
            String processId = Long.toString(processIdInput.longValue());

            // get global session provider instance
            SessionProvider sessionProvider = SessionProvider.INSTANCE.getInstance();

            // check if a logging session already exists - if so, throw an exception
            if (sessionProvider.sessionExists(LoggingSession.SESSION_NAME))
            {
                throw new Exception("A logging session already exists");
            }

            // create REST client and get global values from CR
            ControlRoomApiUtility apiUtility = new ControlRoomApiUtility(this.globalSessionContext);
            String configJson = ((StringValue) apiUtility.getGlobalValue("strLoggingConfig")).get();

            // create logging session object
            LoggingSession logSession = new LoggingSession(processId, configJson);

            // add to global session provider
            sessionProvider.startSession(LoggingSession.SESSION_NAME, logSession);

        } catch (Exception ex)
        {
            ex.printStackTrace();
            throw new BotException(ex);
        }
    }
}
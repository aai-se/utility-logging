package com.automationanywhere.professional_services.logging.botcommand;

import com.automationanywhere.bot.service.BotException;
import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.professional_services.logging.LogLevel;
import com.automationanywhere.professional_services.logging.LoggingSession;
import com.automationanywhere.professional_services.utilities.BotUtilities;
import com.automationanywhere.professional_services.utilities.SessionProvider;

import static com.automationanywhere.commandsdk.model.AttributeType.RADIO;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand
@CommandPkg
        (
                name = "WriteToLog",
                label = "[[WriteToLog.label]]",
                node_label = "[[WriteToLog.node_label]]",
                description = "[[WriteToLog.description]]",
                icon = "pkg.svg"
        )
public class B_WriteToLog
{
    public static final String DEBUG = "DEBUG";
    public static final String INFO = "INFO";
    public static final String ERROR = "ERROR";
    public static final String FATAL = "FATAL";

    @com.automationanywhere.commandsdk.annotations.GlobalSessionContext
    private GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(GlobalSessionContext globalSessionContext)
    {
        this.globalSessionContext = globalSessionContext;
    }

    @Execute
    public void writeLogEntry(

            @Idx(index = "1", type = RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(label = "[[WriteToLog.logType.debug.label]]", value = DEBUG)),
                    @Idx.Option(index = "1.2", pkg = @Pkg(label = "[[WriteToLog.logType.info.label]]", value = INFO)),
                    @Idx.Option(index = "1.3", pkg = @Pkg(label = "[[WriteToLog.logType.error.label]]", value = ERROR)),
                    @Idx.Option(index = "1.4", pkg = @Pkg(label = "[[WriteToLog.logType.fatal.label]]", value = FATAL))
            }
            )
            @Pkg(label = "[[WriteToLog.logType.label]]", default_value_type = STRING, default_value = "DEBUG")
            @NotEmpty
            String logTypeString,

            @Idx(index = "2", type = TEXT)
            @Pkg(label = "[[WriteToLog.logMessage.label]]")
            @NotEmpty
            String logMessage,

            @Idx(index = "3", type = TEXT)
            @Pkg(label = "[[WriteToLog.reference.label]]")
            String reference
    )
    {
        try
        {
            // get global session provider instance
            SessionProvider sessionProvider = SessionProvider.INSTANCE.getInstance();

            // if a logging session does not exist, start one with an ID of -1
            if (!sessionProvider.sessionExists(LoggingSession.SESSION_NAME))
            {
                A_StartLoggingSession startLoggingSessionCommand = new A_StartLoggingSession();
                startLoggingSessionCommand.setGlobalSessionContext(this.globalSessionContext);
                startLoggingSessionCommand.startLoggingSession(-1D);
            }

            // get the logging session object
            LoggingSession session = (LoggingSession) sessionProvider.getSession(LoggingSession.SESSION_NAME);

            // get the taskbot name
            String botName = BotUtilities.getBotName(this.globalSessionContext.getBotUri());

            // convert log type string to enum value
            LogLevel logType = LogLevel.valueOf(logTypeString);

            // write message to log file
            session.executeLogEntry(botName, logType, logMessage, reference);

        } catch (Exception ex)
        {
            throw new BotException(ex);
        }
    }
}
package com.automationanywhere.professional_services.logging.botcommand;

import com.automationanywhere.bot.service.BotException;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.professional_services.logging.LoggingSession;
import com.automationanywhere.professional_services.utilities.SessionProvider;

@BotCommand
@CommandPkg
        (
                name = "EndLoggingSession",
                label = "[[EndLoggingSession.label]]",
                node_label = "[[EndLoggingSession.node_label]]",
                description = "[[EndLoggingSession.description]]",
                icon = "pkg.svg"
        )

public class C_EndLoggingSession
{
    @Execute
    public void endLoggingSession()
    {
        try
        {
            // get global session provider instance
            SessionProvider sessionProvider = SessionProvider.INSTANCE.getInstance();

            // check this logging session name exists
            if (!sessionProvider.sessionExists(LoggingSession.SESSION_NAME))
            {
                throw new Exception("A logging session was not found");
            }

            // get logging session object, close it and remove it from the session provider
            LoggingSession loggingSession = (LoggingSession) sessionProvider.getSession(LoggingSession.SESSION_NAME);
            loggingSession.endSession();
            sessionProvider.endSession(LoggingSession.SESSION_NAME);

        } catch (Exception ex)
        {
            throw new BotException(ex);
        }
    }
}

package com.automationanywhere.professional_services.logging;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.professional_services.utilities.ControlRoomApiUtility;
import com.automationanywhere.professional_services.utilities.converters.JsonConverter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 Main class responsible for handling all logic related to logging.
 */
public class LoggingSession
{

    // constants
    public static final String SESSION_NAME = "Logging";

    // logging config variables
    private Boolean isDebugEnabled;
    private Boolean isLocalLoggingEnabled;
    private Boolean isLocalLogPruningEnabled;
    private Boolean isLoggingCentralisationEnabled;
    private Boolean isLoggingDatabaseEnabled;

    private String localRpaRootFolder;
    private String fileshareRpaRootFolder;
    private String loggingDatabaseConnectionString;
    private String loggingDatabaseTable;
    private String logFileDateFormat;
    private String logFileDateFormatRegex;
    private String logFileTimestampFormat;

    private char logFileSeparator;

    private int logFilePruningDaysToRetain;

    // logging session data
    private String processId;
    private String computerName;
    private String username;
    private String currentDate;

    // logging objects
    private DateTimeFormatter dateFormatter;
    private DateTimeFormatter timestampFormatter;

    private File localLogFile;
    private File fileshareLogFile;

    private PrintWriter localLogFileWriter;

    /**
     Constructor for creation of logging session object.

     @param processId  Unique process ID to be used for the duration of this logging session
     @param configJson JSON containing config parameters required for initialisation of logging session
     */
    public LoggingSession(String processId, String configJson)
            throws Exception
    {
        // store simple input variables
        this.processId = processId;

        // set logging config variables from JSON
        setLoggingConfigVariablesFromConfigJson(configJson);

        // set logging session environment variables
        setLogSessionEnvironmentVariables();

        // initialise objects for use in file-based logging if local logging is enabled
        if (isLocalLoggingEnabled)
        {
            initialiseFileLoggingObjects();
        }

        // initialise objects for use in database logging if database logging is enabled
        // TODO - database logging is not currently implemented, so this has no effect
        if (isLoggingDatabaseEnabled)
        {
            initialiseDatabaseLoggingObjects();
        }
    }

    /**
     Method to configure logging session using config parameters received as JSON input.

     @param configJson Config parameters expressed as string of JSON
     */
    private void setLoggingConfigVariablesFromConfigJson(String configJson)
            throws Exception
    {
        // convert logging config to map
        Map<String, Value> loggingConfigMap = JsonConverter.convertJsonToMapOfValues(configJson);

        // set individual config values

        // booleans
        this.isDebugEnabled = ((BooleanValue) loggingConfigMap.get("isDebugEnabled")).get();
        this.isLocalLoggingEnabled = ((BooleanValue) loggingConfigMap.get("isLocalLoggingEnabled")).get();
        this.isLocalLogPruningEnabled = ((BooleanValue) loggingConfigMap.get("isLocalLogPruningEnabled")).get();
        this.isLoggingCentralisationEnabled = ((BooleanValue) loggingConfigMap.get("isLoggingCentralisationEnabled")).get();
        this.isLoggingDatabaseEnabled = ((BooleanValue) loggingConfigMap.get("isLoggingDatabaseEnabled")).get();

        // strings
        this.localRpaRootFolder = ((StringValue) loggingConfigMap.get("localRpaRootFolder")).get();
        this.fileshareRpaRootFolder = ((StringValue) loggingConfigMap.get("fileshareRpaRootFolder")).get();
        this.loggingDatabaseConnectionString = ((StringValue) loggingConfigMap.get("loggingDatabaseConnectionString")).get();
        this.loggingDatabaseTable = ((StringValue) loggingConfigMap.get("loggingDatabaseTable")).get();
        this.logFileDateFormat = ((StringValue) loggingConfigMap.get("logFileDateFormat")).get();
        this.logFileDateFormatRegex = ((StringValue) loggingConfigMap.get("logFileDateFormatRegex")).get();
        this.logFileTimestampFormat = ((StringValue) loggingConfigMap.get("logFileTimestampFormat")).get();

        // chars
        this.logFileSeparator = ((StringValue) loggingConfigMap.get("logFileSeparator")).get().charAt(0);

        // numbers
        this.logFilePruningDaysToRetain = ((NumberValue) loggingConfigMap.get("logFilePruningDaysToRetain")).get().intValue();

        // update to localRpaRootFolder IF this is variablised
        localRpaRootFolder = ControlRoomApiUtility.replaceVariablisedPath(localRpaRootFolder);
    }

    /**
     Method to configure logging session using Windows environment variables.
     */
    private void setLogSessionEnvironmentVariables()
    {
        // get machine name
        this.computerName = System.getenv("COMPUTERNAME");

        // get user name and domain name
        String username = System.getenv("USERNAME");
        String userDomain = System.getenv("USERDOMAIN");

        // set user name with domain name if available
        if (userDomain.length() > 0)
        {
            this.username = userDomain + "\\" + username;
        } else
        {
            this.username = username;
        }
    }

    /**
     Method to initialise file logging objects for use when writing log entries. Only relevant if local file logging is active.
     */
    private void initialiseFileLoggingObjects()
            throws Exception
    {
        // set datetime formatters
        this.dateFormatter = DateTimeFormatter.ofPattern(logFileDateFormat);
        this.timestampFormatter = DateTimeFormatter.ofPattern(logFileTimestampFormat);
        this.currentDate = LocalDateTime.now().format(dateFormatter);

        // create reference to local file object
        this.localLogFile = getLogFileInFolder(localRpaRootFolder);

        // create parent directories if they do not exist
        this.localLogFile.getParentFile().mkdirs();

        // set flag for file exists
        boolean fileExists = this.localLogFile.exists();

        // create file if it doesn't exist
        if (!fileExists)
        {
            this.localLogFile.createNewFile();
        }

        // create file writer object
        this.localLogFileWriter = new PrintWriter(new FileOutputStream(this.localLogFile, true), true);

        // initialise file with separator and header row if it is new
        if (!fileExists)
        {
            addSeparatorDefinitionToLocalLogFile();
            addHeaderRowToLocalLogFile();
        }

        // create objects required to copy log files centrally only if fileshare centralisation is enabled
        if (isLoggingCentralisationEnabled)
        {
            // create reference to remote file object
            this.fileshareLogFile = getLogFileInFolder(fileshareRpaRootFolder);

            // create parent directories if they do not exist
            this.fileshareLogFile.getParentFile().mkdirs();
        }
    }

    /**
     Method to get a reference to a log file in a specified storage folder. Note - this file is not created by this method.

     @param rootFolder Root folder to use when creating the path to the log file

     @return File object for log file
     */
    private File getLogFileInFolder(String rootFolder)
            throws Exception
    {
        // validate that this path represents a directory
        if (!Paths.get(rootFolder).toFile().isDirectory())
        {
            throw new Exception("Invalid folder: " + rootFolder);
        }

        // concatenate path to log file
        String pathToLocalLogFileInput =
                rootFolder + File.separator + processId + File.separator + "Logs" + File.separator + processId + " - " + currentDate + " - " + computerName + ".csv";

        // return file object
        return Paths.get(pathToLocalLogFileInput).toFile();
    }

    /**
     Convenience method to insert a single line into a log file to define the separator character.
     Enables .csv log files to be opened in Excel natively with any separator character.
     */
    private void addSeparatorDefinitionToLocalLogFile()
    {
        // write separator to file
        this.localLogFileWriter.println("sep=" + logFileSeparator);
    }

    /**
     Convenience method to insert a single line into a log file to define the header row.
     */
    private void addHeaderRowToLocalLogFile()
    {
        // define header row
        String headerRow = "Timestamp" +
                                   logFileSeparator +
                                   "Machine name" +
                                   logFileSeparator +
                                   "Username" +
                                   logFileSeparator +
                                   "Process ID" +
                                   logFileSeparator +
                                   "Task name" +
                                   logFileSeparator +
                                   "Log level" +
                                   logFileSeparator +
                                   "Log message" +
                                   logFileSeparator +
                                   "Reference";

        // write header row to file
        this.localLogFileWriter.println(headerRow);
    }

    /**
     Not implemented.
     */
    private void initialiseDatabaseLoggingObjects()
            throws Exception
    {
        // TODO build code for connecting to database
    }

    /**
     Method to execute writing of a single log entry.

     @param taskbotName Taskbot name
     @param logLevel    Log level
     @param logMessage  Log message
     @param reference   Reference (optional)
     */
    public void executeLogEntry(String taskbotName, LogLevel logLevel, String logMessage, String reference)
    {
        // return immediately if log level is debug but debug is not enabled in this environment
        if (logLevel == LogLevel.DEBUG && !isDebugEnabled)
        {
            return;
        }

        // write log entry to local log file if enabled
        if (isLocalLoggingEnabled)
        {
            writeToLocalLogFile(taskbotName, logLevel, logMessage, reference);
        }

        // write log entry to logging database if enabled
        if (isLoggingDatabaseEnabled)
        {
            writeToLoggingDatabase(taskbotName, logLevel, logMessage, reference);
        }
    }

    /**
     Method to write a log entry to a local log file.

     @param taskbotName Taskbot name
     @param logLevel    Log level
     @param logMessage  Log message
     @param reference   Reference (optional)
     */
    private void writeToLocalLogFile(String taskbotName, LogLevel logLevel, String logMessage, String reference)
    {
        // generate timestamp
        String timestamp = LocalDateTime.now().format(this.timestampFormatter);

        // concatenate log entry
        String logEntry = this.concatenateLogEntry(timestamp, taskbotName, logLevel, logMessage, reference);

        // write log entry to file
        this.localLogFileWriter.println(logEntry);
    }

    /**
     Method to concatenate a single log entry to be written to a file.

     @param timestamp   Timestamp for log entry
     @param taskbotName Taskbot name
     @param logLevel    Log level
     @param logMessage  Log message
     @param reference   Reference (optional)
     */
    private String concatenateLogEntry(String timestamp, String taskbotName, LogLevel logLevel, String logMessage, String reference)
    {
        return timestamp +
                       logFileSeparator +
                       computerName +
                       logFileSeparator +
                       username +
                       logFileSeparator +
                       processId +
                       logFileSeparator +
                       taskbotName +
                       logFileSeparator +
                       logLevel.name() +
                       logFileSeparator +
                       logMessage +
                       logFileSeparator +
                       reference;
    }

    /**
     Method to write a log entry to a database.

     @param taskbotName Taskbot name
     @param logLevel    Log level
     @param logMessage  Log message
     @param reference   Reference (optional)
     */
    private void writeToLoggingDatabase(String taskbotName, LogLevel logLevel, String logMessage, String reference)
    {
        // TODO execute insert into logging database
    }

    /**
     Method to end a logging session.
     */
    public void endSession()
            throws Exception
    {
        // handle log database if enabled
        if (isLoggingDatabaseEnabled)
        {
            // TODO close connections etc
        }

        // handle log files if enabled
        if (isLocalLoggingEnabled)
        {
            // close file writer object
            this.localLogFileWriter.close();

            // check if log file centralisation is enabled
            if (isLoggingCentralisationEnabled)
            {
                // copy file from local path to path on fileshare
                Files.copy(localLogFile.toPath(), fileshareLogFile.toPath(), StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES);
            }

            // check if log file pruning is enabled
            if (isLocalLogPruningEnabled)
            {
                // prune local log files
                pruneLocalLogFiles();
            }
        }

    }

    /**
     Method to prune local log files.
     */
    private void pruneLocalLogFiles()
            throws Exception
    {
        // get reference to local logs folder
        Path pathToLocalRpaRootFolder = Paths.get(this.localRpaRootFolder);

        // validate folder - already done during start session, but worth repeating
        if (!pathToLocalRpaRootFolder.toFile().isDirectory())
        {
            throw new Exception("Invalid folder: " + this.localRpaRootFolder);
        }

        // create regex objects
        Pattern pattern = Pattern.compile(this.logFileDateFormatRegex);
        Matcher matcher = pattern.matcher("");

        // create date object representing retention period cut-off
        LocalDate cutoffDate = LocalDate.now().minus(logFilePruningDaysToRetain, ChronoUnit.DAYS);

        // get list of all Logs subfolders in root RPA folder
        List<Path> logSubfolderPaths =
                Files.walk(pathToLocalRpaRootFolder, 4)
                        .filter(file -> Files.isDirectory(file))
                        .filter(path -> path.toString().endsWith("Logs"))
                        .collect(Collectors.toUnmodifiableList());

        // iterate through list of all subfolders
        for (Path logSubfolderPath : logSubfolderPaths)
        {
            // get CSV files within this subfolder
            List<Path> csvFilePaths = Files.list(logSubfolderPath)
                                              .filter(file -> !Files.isDirectory(file))
                                              .filter(path -> path.toString().endsWith(".csv"))
                                              .collect(Collectors.toList());

            // loop through CSV log files
            for (Path csvFilePath : csvFilePaths)
            {
                // reset matcher object using CSV file path
                matcher.reset(csvFilePath.toString());

                // get date from CSV file - if not found, move to next file
                if (!matcher.find())
                {
                    continue;
                }

                // convert date string to date object
                LocalDate logFileDate = LocalDate.parse(matcher.group(0), this.dateFormatter);

                // check if this file is older than the specified retention period
                if (logFileDate.isBefore(cutoffDate))
                {
                    // file is too old - delete it
                    csvFilePath.toFile().delete();
                }
            }
        }
    }
}
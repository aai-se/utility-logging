package com.automationanywhere.professional_services.utilities;

import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 Convenience class for extracting bot names from URIs.
 */
public class BotUtilities
{
    /*
    Patterns to match bot name in bot URI after URL decoding
    Pattern 1 matches forward slash, then any non-forward slash characters, followed by "?fileId"
    Pattern 2 matches forward slash, then any non-forward slash characters, followed by "fileId"
    Pattern 1 is more conservative; if that fails, pattern 2 should match safely
     */
    public static final String patternBotName1 = "(?<=/)[^/]+(?=\\?fileId)";
    public static final String patternBotName2 = "(?<=/)[^/]+(?=fileId)";

    public static String getBotName(String botUri)
            throws Exception
    {
        String botName = URLDecoder.decode(botUri, Charset.defaultCharset());

        try
        {
            Matcher matcher = Pattern.compile(patternBotName1).matcher(botName);
            matcher.find();
            botName = matcher.group();

        } catch (Exception ex1)
        {
            try
            {
                Matcher matcher = Pattern.compile(patternBotName2).matcher(botName);
                matcher.find();
                botName = matcher.group();
            } catch (Exception ex2)
            {
                throw new Exception("Could not extract bot name! Raw input: " + botUri);
            }
        }

        return botName;
    }
}
package com.automationanywhere.professional_services.utilities;

import java.util.HashMap;
import java.util.Map;

/**
 Command package global session provider class.
 */
public enum SessionProvider
{
    INSTANCE();

    private Map<String, Object> sessions;

    private SessionProvider()
    {
        this.sessions = new HashMap<String, Object>();
    }

    public SessionProvider getInstance()
    {
        return INSTANCE;
    }

    public void startSession(String sessionName, Object sessionContent)
    {
        this.sessions.put(sessionName, sessionContent);
    }

    public Object getSession(String sessionName)
    {
        return this.sessions.get(sessionName);
    }

    public boolean sessionExists(String sessionName)
    {
        return this.sessions.containsKey(sessionName);
    }

    public void endSession(String sessionName)
    {
        this.sessions.remove(sessionName);
    }
}
